package com.evans;

public class Keys {
        //Declare Variables
        private String blank;
        private String server;
        private String acao;
        private String pragma;
        private String date;
        private String acah;
        private String aspMvcVersion;
        private String cacheControl;
        private String aspVersion;
        private String setCookie;
        private String expires;
        private String contentLength;
        private String poweredBy;
        private String contentType;

    public String getBlank() {
        return blank;
    }

    public void setBlank(String blank) {
        this.blank = blank;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getPragma() {
        return pragma;
    }

    public void setPragma(String pragma) {
        this.pragma = pragma;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAcah() {
        return acah;
    }

    public void setAcah(String acah) {
        this.acah = acah;
    }

    public String getAspMvcVersion() {
        return aspMvcVersion;
    }

    public void setAspMvcVersion(String aspMvcVersion) {
        this.aspMvcVersion = aspMvcVersion;
    }

    public String getCacheControl() {
        return cacheControl;
    }

    public void setCacheControl(String cacheControl) {
        this.cacheControl = cacheControl;
    }

    public String getAspVersion() {
        return aspVersion;
    }

    public void setAspVersion(String aspVersion) {
        this.aspVersion = aspVersion;
    }

    public String getSetCookie() {
        return setCookie;
    }

    public void setSetCookie(String setCookie) {
        this.setCookie = setCookie;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getContentLength() {
        return contentLength;
    }

    public void setContentLength(String contentLength) {
        this.contentLength = contentLength;
    }

    public String getPoweredBy() {
        return poweredBy;
    }

    public void setPoweredBy(String poweredBy) {
        this.poweredBy = poweredBy;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public String toString() {
        return "Keys{" +
                "blank='" + blank + '\'' +
                ", server='" + server + '\'' +
                ", acao='" + acao + '\'' +
                ", pragma='" + pragma + '\'' +
                ", date='" + date + '\'' +
                ", acah='" + acah + '\'' +
                ", aspMvcVersion='" + aspMvcVersion + '\'' +
                ", cacheControl='" + cacheControl + '\'' +
                ", aspVersion='" + aspVersion + '\'' +
                ", setCookie='" + setCookie + '\'' +
                ", expires='" + expires + '\'' +
                ", contentLength='" + contentLength + '\'' +
                ", poweredBy='" + poweredBy + '\'' +
                ", contentType='" + contentType + '\'' +
                '}';
    }
}
