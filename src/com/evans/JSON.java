package com.evans;

import  com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class JSON {
    public static String keysToJSON(Keys keys) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(keys);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }
    public static Keys JSONtoKeys(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Keys keys = null;

        try {
            keys = mapper.readValue(s, Keys.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return  keys;
    }
}
