package com.evans;

import java.util.Map;

public class Main {

    public static void main(String[] args) {
        // Declare Variables
        String keyValue = "";
        int keysSet = 0;
        String[] keysArray1 = {"", "", "", "", "", "", "", "", "", "", "", "", "", ""};

        Keys keys = new Keys();

        Map<Integer, String> m = HTTP.getHttpHeaders("https://www.byui.edu/");
        for (Map.Entry<Integer, String> entry : m.entrySet()) {
            keyValue = String.valueOf(entry.getKey());
            keysArray1[keysSet] = keyValue;
            System.out.println(("Key = ") + entry.getKey()); // + " Value = " + entry.getValue()); // Print Headers
            keysSet = keysSet + 1;
        }

        //I'm sure there is an easier way to do this but I Have been working on a solution and this is what I came up
        // with. Not very efficient but it works.
        keys.setBlank(keysArray1[0]);
        keys.setServer(keysArray1[1]);
        keys.setAcao(keysArray1[2]);
        keys.setPragma(keysArray1[3]);
        keys.setDate(keysArray1[4]);
        keys.setAcah(keysArray1[5]);
        keys.setAspMvcVersion(keysArray1[6]);
        keys.setCacheControl(keysArray1[7]);
        keys.setAspVersion(keysArray1[8]);
        keys.setSetCookie(keysArray1[9]);
        keys.setExpires(keysArray1[10]);
        keys.setContentLength(keysArray1[11]);
        keys.setPoweredBy(keysArray1[12]);
        keys.setContentType(keysArray1[13]);

        String json = JSON.keysToJSON(keys);
        System.out.println("Website Headers to JSON: " + json);

        Keys keys2 = JSON.JSONtoKeys(json);
        System.out.println("JSON to Website Headers: " + keys2);

        //System.out.println("Keys to String: " + keys.toString());
    }
}
